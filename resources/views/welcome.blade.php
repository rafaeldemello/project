<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Atividades
                </div>
                <div>
                  <pre>
                    Teste de Conhecimentos – Analista

                    Desenvolvedor

                    Os exercícios propostos a seguir, não possuem apenas uma forma correta de resolução. Aplique
                    seus conhecimentos de forma a fazer uso das boas práticas de programação, sempre prezando
                    pela qualidade e simplicidade.
                    Desta forma, além de um exercício de lógica de programação, esperamos que você exercite sua
                    criatividade e procure nos surpreender.
                    Boa sorte!

                    INTRODUÇÃO
                    Leia atentamente as instruções e desenvolva as soluções conforme proposto. Serão
                    consideradas para a avaliação somente as questões que respeitarem os requisitos.

                    AVALIAÇÃO
                    A prova será avaliada de acordo com critérios de qualidade de software e design de código.
                    Buscamos verificar se os conhecimentos e a forma que você os coloca em prática condizem
                    com nossas expectativas.
                    Com relação a análise de requisitos, arquitetura e modelagem de dados, inclua na entrega, os
                    artefatos e especificações que achar necessário.
                    Nossa intenção é entender melhor o seu nível de conhecimento nos aspectos que julgamos
                    importantes e que são requisitos da vaga que você se candidatou.
                    REQUISITOS
                    • Hospedar o código no GitHub (https://github.com/) ou Bitbucket (https://bitbucket.org/)
                    • Linguagem PHP 5.3
                    • Siga todas as regras definidas pela PSR-2
                    • Frameworks e bibliotecas utilizados para solução devem estar inclusos no repositório
                    • O repositório deve possuir um README.md, contendo instruções de como rodar o projeto
                    • Todas as questões devem constar no repositório

                    QUESTÕES
                    1. Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima
                    “Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos
                    de ambos (3 e 5), imprima “FizzBuzz”.
                    2. Refatore o código abaixo, fazendo as alterações que julgar necessário.
                    1. < ?
                    2.
                    3. if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

                    4. header("Location: http://www.google.com");
                    5. exit();
                    6. } elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
                    7. header("Location: http://www.google.com");
                    8. exit();
                    9. }

                    3. Refatore o código abaixo, fazendo as alterações que julgar necessário.
                    1. < ? php
                    2.
                    3. class MyUserClass
                    4. {
                    5. public function getUserList()
                    6. {
                    7. $dbconn = new DatabaseConnection('localhost','user','password');
                    8. $results = $dbconn->query('select name from user');
                    9.
                    10. sort($results);
                    11.
                    12. return $results;
                    13. }
                    14. }

                    4. Desenvolva uma API Rest para um sistema gerenciador de tarefas
                    (inclusão/alteração/exclusão). As tarefas consistem em título e descrição, ordenadas por
                    prioridade.
                    Desenvolver utilizando:
                    • Linguagem PHP (ou framework CakePHP);
                    • Banco de dados MySQL;
                    Diferenciais:
                    • Criação de interface para visualização da lista de tarefas;
                    • Interface com drag and drop;
                    • Interface responsiva (desktop e mobile);
                  </pre>
                </div>

            </div>
        </div>
    </body>
</html>
