@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Intro</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bem vindo {{ $username }}, abaixo estão as atividades solicitadas

                </div>
            </div>
            <div class="card">
                <div class="card-header">Atividade #01</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                          @for ($i = 1; $i <= 100; $i++)
                             @if ($i % 3 == 0 && $i % 5 != 0)
                             Fizz /
                             @elseif ($i % 5 == 0 && $i % 3 != 0)
                             Buzz /
                             @elseif ($i % 3 == 0 && $i % 5 == 0)
                             FizzBuzz
                             @else
                              {{ $i != 100 ? '/' . $i : $i  }}
                             @endif
                          @endfor
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Atividade #02</div>

                <div class="card-body">
                    <div class="row">
                        <h3>Middlewares</h3>
                        <p>
                            <strong>Não aplicado a funcionalidade abaixo por questões de padrão e   utilização do framework Laravel. </strong>
                        </p>
                        <p>
                            if (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
                                 header("Location: http://www.google.com");
                                 exit();
                            }
                            elseif (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
                                header("Location: http://www.google.com");
                                exit();
                            }
                        </p>

                        <p>
                            <strong>Mantido o padrão tratativo dos Middlewares para acesso e autenticação, utilizando o <a target="_blank" href="https://laravel.com/docs/5.8/authentication">Laravel Auth</a>, que cria e gerencia as rotinas de acesso.</strong>
                        </p>
                        <p>
                            (<strong>App/Http/Middlevare/Authenticate.php</strong>)
                            if (! $request->expectsJson()) {
                                return route('login');
                            }
                        </p>
                        <p>
                            (<strong>App/Http/Middlevare/RedirectifAuthenticate.php</strong>)
                            if (Auth::guard($guard)->check()) {
                                return redirect('/home');
                            }
                        </p>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-header">Atividade #03</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Users List (Utilizando o ORM <a target="_blank" href="https://laravel.com/docs/5.8/eloquent">Eloquent</a></h3>
                            <p>
                                public function getUserList()
                                {
                                    $dbconn = new DatabaseConnection('localhost','user','password');
                                    $results = $dbconn->query('select name from user');

                                    sort($results);

                                    return $results;
                                }
                            </p>
                            <p><strong>PARA</strong></p>
                            <p>
                                $users = \App\User::all();
                            </p>
                        </div>
                        <div class="col-md-12">
                            <h4>Lista de Users</h4>
                            <table class="table">
                                <thead>
                                  <tr>
                                      <th>Nome</th>
                                      <th>Email</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email ? $user->email : '-'}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Todo List</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="name">Prioridade</label>
                                    <select id="priority" name="priority">
                                        <option value="1">Baixo</option>
                                        <option value="2">Médio</option>
                                        <option value="3">Alto</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="name">Tarefa*</label>
                                    <input type="hidden" id="id" name="id">
                                    <input type="text" id="name" name="name">
                                </div>
                                <div class="col-md-4">
                                    <label for="name">Descrição*</label>
                                    <input type="text" id="text" name="text">
                                </div>
                                <div class="col-md-12">
                                    <button onclick="addTodo(event)">Salvar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <h4>Todo List</h4>
                          <table class="table">
                              <thead>
                                <tr>
                                  <th>
                                    Prioridade
                                  </th>
                                  <th>
                                    Tarefa
                                  </th>
                                  <th>
                                    Descrição
                                  </th>
                                  <th>
                                  </th>
                                <tr>
                                </thead>
                                <tbody id="body">
                                </tbody>
                            </table>
                          </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript">

    $('body').on('click', '.remove', function(e) {
      //deleteTodo();
    })

    function deleteTodo(id) {
      console.log('apag');
        $.ajax({
           url: '/todo-delete',
           dataType:'json',
           type: 'delete',
           data: {id: id},
           success:function(data){
               listTodo();
           }
        });
    }

    function getTodo(id) {
      $.ajax({
         url: '/todo-by-id',
         dataType:'json',
         type: 'get',
         data: {id: id},
         success:function(data){
             $('#name').val(data.name);
             $('#priority').val(data.priority);
             $('#text').val(data.text);
             $('#id').val(data.id);
             $('#name').focus();
         }
      });
    }

    function  updateTodo(todo) {
      console.log(todo)
    }

    function mountTable(data) {
      for (var i in data) {
        var row = '<tr><td>'+ data[i].priority  +'</td><td>'+ data[i].name +'</td><td>'+ data[i].text +'</td><td><button class="remove btn" onclick="deleteTodo('+data[i].id+')">apagar</button><button class="alter btn" onclick="getTodo('+data[i].id+')">alterar</button></td></tr>'
        $('#body').append(row)
      }
    }

    function listTodo() {
      $('#body tr').remove();
         $.ajax({
            url: '/todos',
            dataType:'json',
            type: 'get',
            success:function(data){
                mountTable(data);
            }
        });
    }

    listTodo();


    function addTodo(e) {
        e.preventDefault();

        if (!$('#name').val() || !$('#text').val()) {
          alert('Prenncha todos os campos obrigatórios')
          return false
        }

        var obj = {
            id: $('#id').val(),
            name: $('#name').val(),
            text: $('#text').val(),
            priority: $('#priority').val()
        }

         $.ajax({
            url: $('#id').val() ? '/todo-update' : 'todos',
            dataType:'json',
            type: $('#id').val() ? 'put' : 'post',
            data: obj,
            success:function(data){
                if ($('#id').val()) {
                  alert('alterado com sucesso');
                } else {
                  alert('salvo com sucesso');
                }
                $('#id').val('');
                $('#name').val('');
                $('#text').val('');
                listTodo();
            }
        });
    }





</script>
