<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->get('id')) {
          $response = \App\Todo::find($request->get('id'));
      } else {
          $response = DB::table('todos')->orderBy('priority', 'DESC')->get();
      }
      // \App\Todo::orderBy('priority', 'DESC')->all();
      return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $todo = $request->all();
        // var_dump($todo);

        $todo_obj = new \App\Todo;

        // var_dump($todo['name']);
        $todo_obj->name = $todo['name'];
        $todo_obj->priority = $todo['priority'];
        $todo_obj->text = $todo['text'];
        $todo_obj->save();

        $todos = DB::table('todos')->orderBy('priority', 'DESC')->get();
        // \App\Todo::orderBy('priority', 'DESC')->all();

        return response()->json($todos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $todo = \App\Todo::find($request->get('id'));
        $todo->name = $request->get('name');
        $todo->text = $request->get('text');
        $todo->priority = $request->get('priority');
        $todo->save();

        return response()->json('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      // var_dump($request->get('id'));
      // echo 'mainha';
      $todo = \App\Todo::find($request->get('id'));

      $todo->delete();
      return response()->json('removed');
        //
    }
}
