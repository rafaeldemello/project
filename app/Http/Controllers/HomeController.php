<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $r)
    {   
        /*Utilizado Eloquent (ORM)*/

        /*DE*/
        
        // public function getUserList()
        // {
        //     $dbconn = new DatabaseConnection('localhost','user','password');
        //     $results = $dbconn->query('select name from user');

        //     sort($results);

        //     return $results;
        // }

        /*PARA*/

        $users = \App\User::all();

        /*##**/

        $username = Auth::user()->name;
        return view('home', array('username' => $username, 'users' => $users));
    }

}
