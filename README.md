# Project

PHP 7.*
Jquery 3.4.*
Mysql/Postgres


Criar o arquivo .env na raiz no projeto com as configurações abaixo

#####

APP_NAME=Atividades
APP_ENV=local
APP_KEY=base64:CjGQT53nQVbr6Jb/miqgsr4SxUiegBSD5Y/Pu8s4TT8=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=project
DB_USERNAME=username
DB_PASSWORD=password

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120


#####

Ao clonar o reposítório executar os comandos do Artisan dentro do projeto

php arstisan key:generate

php artisan migration

php artisan serve

Acessar o http://localhost:8000
